﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data.SqlTypes;
using System.Windows.Forms;
using System.Data;

namespace Classes
{

    public class Contas
    {
        //Properties e fields

        private string _Email;
        private string _Setor;
        private string _Nome;
        private string _Cargo;
        private bool _Administrador;
        private string _Usuario;
        private string _Senha;
        public string Senha
        {
            get
            {
                return _Senha;
            }
            set
            {
                _Senha = value;
            }
        }

        public string Usuario
        {
            get
            {
                return _Usuario;
            }
            set
            {
                _Usuario = value;
            }
        }

        public bool Administrador
        {
            get
            {
                return _Administrador;
            }
            set
            {
                _Administrador = value;
            }
        }

        public string Cargo
        {
            get
            {
                return _Cargo;
            }
            set
            {
                _Cargo = value;
            }
        }

        public string Nome
        {
            get
            {
                return _Nome;
            }
            set
            {
                _Nome = value;
            }
        }

        public string Setor
        {
            get
            {
                return _Setor;
            }
            set
            {
                _Setor = value;
            }
        }

        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
            }
        }



        public bool UsuarioExiste(string user, string senha)
        {

            try
            {
                SqlConnection con = BancoDados.Criarconexao();

                con.Open();

                var query = "SELECT COUNT(*) FROM GSCUsuarios WHERE usuario = @usuario and senha= @senha";

                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@usuario", user);
                cmd.Parameters.AddWithValue("@senha", senha);

                if (int.Parse(cmd.ExecuteScalar().ToString()) == 1)
                {
                    return true;
                }

                cmd.Dispose();
                con.Close();
                con.Dispose();

                return true;
            }
            catch (SqlException SqlEx)
            {
                DialogResult dr = MessageBox.Show("O GSC não conseguiu se contectar com a base de dados. Deseja Tentar Novamente?",
                "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Error);

                if (dr.ToString() == "Yes"){
                    UsuarioExiste(Usuario, Senha);
                }
                          
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }         
        }

        public object getContaAtributo()
        {
            try
            {
                SqlConnection con = BancoDados.Criarconexao();
                SqlDataReader reader;
                con.Open();

                var query = "SELECT nome, setor, email, cargo,administrador FROM GSCUsuarios WHERE usuario = @usuario";

                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@usuario", Usuario);
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Nome = reader.GetString(0);
                    Setor = reader.GetString(1);
                    Email = reader.GetString(2);
                    Cargo = reader.GetString(3);
                    Administrador = reader.GetBoolean(4);
                }

                reader.Close();
                reader.Dispose();
                cmd.Dispose();

                //Fecha a conexão ao final pois ela não é mais necessária
                con.Close();
                con.Dispose();

                return true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool addConta()
        {
            try
            {
                int adm;
                if (Administrador == false)
                    adm = 0;
                else
                    adm = 1;

                var query = new StringBuilder();
                query.Append("INSERT INTO GSCUsuarios (usuario,senha,administrador,email,cargo,setor,nome)");
                query.Append("VALUES(@usuario, @senha, @administrador, @email, @cargo, @setor, @nome)");

                SqlConnection con = BancoDados.Criarconexao();

                con.Open();

                SqlCommand cmd = new SqlCommand(query.ToString(), con);
                cmd.Parameters.AddWithValue("@usuario", Usuario);
                cmd.Parameters.AddWithValue("@senha", Senha);
                cmd.Parameters.AddWithValue("@administrador", adm);
                cmd.Parameters.AddWithValue("@email", Email);
                cmd.Parameters.AddWithValue("@cargo", Cargo);
                cmd.Parameters.AddWithValue("@setor", Setor);
                cmd.Parameters.AddWithValue("@nome", Nome);

                cmd.ExecuteNonQuery();

                cmd.Dispose();
                con.Close();
                con.Dispose();

                return true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void delConta(string userDelete)
        {
            var query = "DELETE FROM GSCUsuarios WHERE usuario = @usuario";

            SqlConnection con = BancoDados.Criarconexao();

            con.Open();

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@usuario", userDelete);

            cmd.ExecuteNonQuery();

            cmd.Dispose();
            con.Close();
            con.Dispose();
        }

        public void getAllUsuarios(DataGridView dataGrid)
        {
            try
            {
                string query = "SELECT nome,usuario,email,setor,cargo,administrador FROM GSCUsuarios";
                SqlConnection con = BancoDados.Criarconexao();

                con.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(query, con);

                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds);
                dataGrid.ReadOnly = true;
                dataGrid.DataSource = ds.Tables[0];

                commandBuilder.Dispose();
                con.Close();
                con.Dispose();

            }
            catch (SqlException sqlEx)
            {
                MessageBox.Show(sqlEx.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void getLikeConta(string parametroWhere, string parametroCondicao, DataGridView dataGrid)
        {

            var query = string.Format("SELECT nome,usuario,email,setor,cargo,administrador FROM GSCUsuarios WHERE {0} LIKE @parametroCondicao", parametroWhere);

            SqlConnection con = BancoDados.Criarconexao();

            con.Open();

            SqlDataAdapter dataAdapter = new SqlDataAdapter(query, con);
            dataAdapter.SelectCommand.Parameters.AddWithValue("parametroCondicao", parametroCondicao + "%");

            DataTable ds = new DataTable();
            dataAdapter.Fill(ds);
            dataGrid.ReadOnly = true;
            dataGrid.DataSource = ds;

            dataAdapter.Dispose();
            con.Close();
            con.Dispose();
        }

        public bool updateConta()
        {

            try
            {
                var query = new StringBuilder();
                query.Append("UPDATE GSCUsuarios SET nome = @nome,usuario = @usuario,email = @email,setor = @setor," +
                             "cargo = @cargo,administrador = @adm WHERE usuario = @usuario");

                SqlConnection con = BancoDados.Criarconexao();

                con.Open();

                SqlCommand cmd = new SqlCommand(query.ToString(), con);

                cmd.Parameters.AddWithValue("@nome", Nome);
                cmd.Parameters.AddWithValue("@usuario", Usuario);
                cmd.Parameters.AddWithValue("@email", Email);
                cmd.Parameters.AddWithValue("@setor", Setor);
                cmd.Parameters.AddWithValue("@cargo", Cargo);
                cmd.Parameters.AddWithValue("@adm", Administrador);

                cmd.ExecuteNonQuery();
                cmd.Dispose();
                cmd.Clone();

                return true;
            }
            catch (SqlException sqlEx)
            {
                MessageBox.Show(sqlEx.Message, "Erro na Atualização da Base de Dados");
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
