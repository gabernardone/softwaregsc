﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Classes
{
    public class Funcionario
    {
        private int _id_funcionario;

        public int Id_funcionario
        {
            get { return _id_funcionario; }
            set { _id_funcionario = value; }
        }
        private int _cargo;

        public int Cargo
        {
            get { return _cargo; }
            set { _cargo = value; }
        }
        private string _nome;

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }
        private int _setor;

        public int Setor
        {
            get { return _setor; }
            set { _setor = value; }
        }
        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
    }
}
