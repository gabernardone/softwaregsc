﻿namespace Windows
{
    partial class frmLogin
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">verdade se for necessário descartar os recursos gerenciados; caso contrário, falso.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte do Designer - não modifique
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Windows.SplashScreen1), true, true, true);
            this.picLogoWindows = new System.Windows.Forms.PictureBox();
            this.gpbLogin = new System.Windows.Forms.GroupBox();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnEntrar = new DevExpress.XtraEditors.SimpleButton();
            this.panSenha = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.picShowHide = new System.Windows.Forms.PictureBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.panUsuario = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.panErro = new System.Windows.Forms.Panel();
            this.picErro = new System.Windows.Forms.PictureBox();
            this.lblErro = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.lblSenha = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.retsUsuario = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.retsSenha = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblLogin_texto = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.panFormLogin = new System.Windows.Forms.Panel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.picStatusConexao = new System.Windows.Forms.PictureBox();
            this.lblStatus1 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TimerConexao = new System.Windows.Forms.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.picLogoWindows)).BeginInit();
            this.gpbLogin.SuspendLayout();
            this.panSenha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picShowHide)).BeginInit();
            this.panUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panErro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picErro)).BeginInit();
            this.panFormLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStatusConexao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // picLogoWindows
            // 
            this.picLogoWindows.BackColor = System.Drawing.Color.Transparent;
            this.picLogoWindows.Image = global::Windows.Properties.Resources.logobig;
            this.picLogoWindows.Location = new System.Drawing.Point(94, 24);
            this.picLogoWindows.Name = "picLogoWindows";
            this.picLogoWindows.Size = new System.Drawing.Size(222, 60);
            this.picLogoWindows.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogoWindows.TabIndex = 8;
            this.picLogoWindows.TabStop = false;
            // 
            // gpbLogin
            // 
            this.gpbLogin.BackColor = System.Drawing.Color.Transparent;
            this.gpbLogin.Controls.Add(this.btnCancelar);
            this.gpbLogin.Controls.Add(this.btnEntrar);
            this.gpbLogin.Controls.Add(this.panSenha);
            this.gpbLogin.Controls.Add(this.panUsuario);
            this.gpbLogin.Controls.Add(this.panErro);
            this.gpbLogin.Controls.Add(this.linkLabel1);
            this.gpbLogin.Controls.Add(this.lblSenha);
            this.gpbLogin.Controls.Add(this.lblUsuario);
            this.gpbLogin.Controls.Add(this.shapeContainer1);
            this.gpbLogin.Location = new System.Drawing.Point(11, 182);
            this.gpbLogin.Name = "gpbLogin";
            this.gpbLogin.Size = new System.Drawing.Size(387, 159);
            this.gpbLogin.TabIndex = 9;
            this.gpbLogin.TabStop = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(100, 115);
            this.btnCancelar.LookAndFeel.SkinName = "Office 2013 Light Gray";
            this.btnCancelar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 4;
            this.btnCancelar.Text = "Cancelar";
            // 
            // btnEntrar
            // 
            this.btnEntrar.Location = new System.Drawing.Point(236, 114);
            this.btnEntrar.LookAndFeel.SkinName = "Office 2013 Light Gray";
            this.btnEntrar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(75, 23);
            this.btnEntrar.TabIndex = 3;
            this.btnEntrar.Text = "Entrar";
            this.btnEntrar.Click += new System.EventHandler(this.btnEntrar_Click);
            // 
            // panSenha
            // 
            this.panSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panSenha.Controls.Add(this.pictureBox2);
            this.panSenha.Controls.Add(this.picShowHide);
            this.panSenha.Controls.Add(this.txtSenha);
            this.panSenha.Location = new System.Drawing.Point(130, 70);
            this.panSenha.Name = "panSenha";
            this.panSenha.Size = new System.Drawing.Size(169, 20);
            this.panSenha.TabIndex = 23;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Windows.Properties.Resources.keys_16;
            this.pictureBox2.Location = new System.Drawing.Point(0, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(18, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            // 
            // picShowHide
            // 
            this.picShowHide.Image = global::Windows.Properties.Resources.Show_16x16;
            this.picShowHide.Location = new System.Drawing.Point(148, 1);
            this.picShowHide.Name = "picShowHide";
            this.picShowHide.Size = new System.Drawing.Size(18, 16);
            this.picShowHide.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picShowHide.TabIndex = 24;
            this.picShowHide.TabStop = false;
            this.picShowHide.Click += new System.EventHandler(this.picShowHide_Click);
            // 
            // txtSenha
            // 
            this.txtSenha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSenha.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenha.Location = new System.Drawing.Point(20, 2);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.ShortcutsEnabled = false;
            this.txtSenha.Size = new System.Drawing.Size(126, 15);
            this.txtSenha.TabIndex = 2;
            this.txtSenha.UseSystemPasswordChar = true;
            this.txtSenha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSenha_KeyDown);
            // 
            // panUsuario
            // 
            this.panUsuario.BackColor = System.Drawing.Color.White;
            this.panUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panUsuario.Controls.Add(this.pictureBox3);
            this.panUsuario.Controls.Add(this.txtUsuario);
            this.panUsuario.Location = new System.Drawing.Point(131, 31);
            this.panUsuario.Name = "panUsuario";
            this.panUsuario.Size = new System.Drawing.Size(169, 20);
            this.panUsuario.TabIndex = 22;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Windows.Properties.Resources.Customer_16x16;
            this.pictureBox3.Location = new System.Drawing.Point(0, 1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(18, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 25;
            this.pictureBox3.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUsuario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(22, 2);
            this.txtUsuario.Margin = new System.Windows.Forms.Padding(20);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(143, 15);
            this.txtUsuario.TabIndex = 1;
            this.txtUsuario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSenha_KeyDown);
            // 
            // panErro
            // 
            this.panErro.Controls.Add(this.picErro);
            this.panErro.Controls.Add(this.lblErro);
            this.panErro.Location = new System.Drawing.Point(85, 109);
            this.panErro.Name = "panErro";
            this.panErro.Size = new System.Drawing.Size(230, 21);
            this.panErro.TabIndex = 20;
            this.panErro.Visible = false;
            // 
            // picErro
            // 
            this.picErro.Image = ((System.Drawing.Image)(resources.GetObject("picErro.Image")));
            this.picErro.Location = new System.Drawing.Point(15, 2);
            this.picErro.Name = "picErro";
            this.picErro.Size = new System.Drawing.Size(16, 16);
            this.picErro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picErro.TabIndex = 20;
            this.picErro.TabStop = false;
            // 
            // lblErro
            // 
            this.lblErro.AutoSize = true;
            this.lblErro.Font = new System.Drawing.Font("Segoe UI Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErro.Location = new System.Drawing.Point(33, 4);
            this.lblErro.Name = "lblErro";
            this.lblErro.Size = new System.Drawing.Size(32, 13);
            this.lblErro.TabIndex = 21;
            this.lblErro.Text = "label1";
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.Color.Black;
            this.linkLabel1.Location = new System.Drawing.Point(127, 96);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(113, 13);
            this.linkLabel1.TabIndex = 14;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Esqueceu sua senha?";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.Font = new System.Drawing.Font("Calibri Light", 9.75F);
            this.lblSenha.Location = new System.Drawing.Point(70, 73);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(42, 15);
            this.lblSenha.TabIndex = 11;
            this.lblSenha.Text = "Senha:";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Calibri Light", 9.75F);
            this.lblUsuario.Location = new System.Drawing.Point(70, 34);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(50, 15);
            this.lblUsuario.TabIndex = 9;
            this.lblUsuario.Text = "Usuário:";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.retsUsuario,
            this.retsSenha});
            this.shapeContainer1.Size = new System.Drawing.Size(381, 140);
            this.shapeContainer1.TabIndex = 12;
            this.shapeContainer1.TabStop = false;
            // 
            // retsUsuario
            // 
            this.retsUsuario.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.retsUsuario.BorderWidth = 4;
            this.retsUsuario.Location = new System.Drawing.Point(128, 15);
            this.retsUsuario.Name = "retsUsuario";
            this.retsUsuario.Size = new System.Drawing.Size(169, 20);
            this.retsUsuario.Visible = false;
            // 
            // retsSenha
            // 
            this.retsSenha.BackColor = System.Drawing.Color.White;
            this.retsSenha.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.retsSenha.BorderWidth = 4;
            this.retsSenha.Location = new System.Drawing.Point(127, 54);
            this.retsSenha.Name = "retsSenha";
            this.retsSenha.Size = new System.Drawing.Size(169, 20);
            this.retsSenha.Visible = false;
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.Transparent;
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.Image = global::Windows.Properties.Resources.minimize_ico;
            this.btnMinimize.Location = new System.Drawing.Point(340, 1);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(30, 24);
            this.btnMinimize.TabIndex = 6;
            this.btnMinimize.UseVisualStyleBackColor = false;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Image = global::Windows.Properties.Resources.close_ico;
            this.btnClose.Location = new System.Drawing.Point(378, 1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(30, 24);
            this.btnClose.TabIndex = 5;
            this.btnClose.Tag = "";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblLogin_texto
            // 
            this.lblLogin_texto.AutoSize = true;
            this.lblLogin_texto.BackColor = System.Drawing.Color.Transparent;
            this.lblLogin_texto.Font = new System.Drawing.Font("Calibri Light", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin_texto.Location = new System.Drawing.Point(4, 148);
            this.lblLogin_texto.Name = "lblLogin_texto";
            this.lblLogin_texto.Size = new System.Drawing.Size(142, 33);
            this.lblLogin_texto.TabIndex = 20;
            this.lblLogin_texto.Text = "Faça o login";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Size = new System.Drawing.Size(411, 402);
            this.shapeContainer2.TabIndex = 23;
            this.shapeContainer2.TabStop = false;
            // 
            // panFormLogin
            // 
            this.panFormLogin.BackColor = System.Drawing.Color.Transparent;
            this.panFormLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panFormLogin.Controls.Add(this.lblVersion);
            this.panFormLogin.Controls.Add(this.picStatusConexao);
            this.panFormLogin.Controls.Add(this.lblStatus1);
            this.panFormLogin.Controls.Add(this.lblLogin_texto);
            this.panFormLogin.Controls.Add(this.pictureBox6);
            this.panFormLogin.Controls.Add(this.gpbLogin);
            this.panFormLogin.Controls.Add(this.btnClose);
            this.panFormLogin.Controls.Add(this.btnMinimize);
            this.panFormLogin.Controls.Add(this.picLogoWindows);
            this.panFormLogin.Controls.Add(this.shapeContainer3);
            this.panFormLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panFormLogin.Location = new System.Drawing.Point(0, 0);
            this.panFormLogin.Name = "panFormLogin";
            this.panFormLogin.Size = new System.Drawing.Size(411, 402);
            this.panFormLogin.TabIndex = 22;
            this.panFormLogin.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLogin_MouseDown);
            this.panFormLogin.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmLogin_MouseMove);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Calibri Light", 9.75F);
            this.lblVersion.Location = new System.Drawing.Point(325, 384);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(58, 15);
            this.lblVersion.TabIndex = 23;
            this.lblVersion.Text = "lblVersion";
            // 
            // picStatusConexao
            // 
            this.picStatusConexao.Image = global::Windows.Properties.Resources.status_busy;
            this.picStatusConexao.Location = new System.Drawing.Point(10, 346);
            this.picStatusConexao.Name = "picStatusConexao";
            this.picStatusConexao.Size = new System.Drawing.Size(17, 16);
            this.picStatusConexao.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picStatusConexao.TabIndex = 22;
            this.picStatusConexao.TabStop = false;
            // 
            // lblStatus1
            // 
            this.lblStatus1.AutoSize = true;
            this.lblStatus1.Font = new System.Drawing.Font("Calibri Light", 9.75F);
            this.lblStatus1.Location = new System.Drawing.Point(27, 346);
            this.lblStatus1.Name = "lblStatus1";
            this.lblStatus1.Size = new System.Drawing.Size(126, 15);
            this.lblStatus1.TabIndex = 21;
            this.lblStatus1.Text = "Verificando conexão...!";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(4, 91);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(406, 9);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 20;
            this.pictureBox6.TabStop = false;
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer3.Size = new System.Drawing.Size(409, 400);
            this.shapeContainer3.TabIndex = 0;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.Color.Silver;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 6;
            this.lineShape1.X2 = 401;
            this.lineShape1.Y1 = 366;
            this.lineShape1.Y2 = 366;
            // 
            // TimerConexao
            // 
            this.TimerConexao.Enabled = true;
            this.TimerConexao.Interval = 1;
            this.TimerConexao.Tick += new System.EventHandler(this.TimerConexao_Tick);
            // 
            // splashScreenManager1
            // 
            splashScreenManager1.ClosingDelay = 500;
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(411, 402);
            this.Controls.Add(this.panFormLogin);
            this.Controls.Add(this.shapeContainer2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login - Gestão de Solicitação e Confirmação";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLogin_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmLogin_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.picLogoWindows)).EndInit();
            this.gpbLogin.ResumeLayout(false);
            this.gpbLogin.PerformLayout();
            this.panSenha.ResumeLayout(false);
            this.panSenha.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picShowHide)).EndInit();
            this.panUsuario.ResumeLayout(false);
            this.panUsuario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panErro.ResumeLayout(false);
            this.panErro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picErro)).EndInit();
            this.panFormLogin.ResumeLayout(false);
            this.panFormLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStatusConexao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picLogoWindows;
        private System.Windows.Forms.GroupBox gpbLogin;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.Label lblUsuario;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape retsSenha;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape retsUsuario;
        internal System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.LinkLabel linkLabel1;
        internal System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblLogin_texto;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Panel panFormLogin;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Panel panErro;
        private System.Windows.Forms.Label lblErro;
        private System.Windows.Forms.PictureBox picErro;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panUsuario;
        private System.Windows.Forms.Panel panSenha;
        private DevExpress.XtraEditors.SimpleButton btnEntrar;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Label lblStatus1;
        private System.Windows.Forms.PictureBox picStatusConexao;
        private System.Windows.Forms.Timer TimerConexao;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.PictureBox picShowHide;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}

