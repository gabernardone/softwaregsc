﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Classes;
using DevExpress.XtraSplashScreen;
using Windows.Users_Constrols;
namespace Windows
{
    public partial class ucEditarUsuario : UserControl
    {
        Contas conta = new Contas();


        public ucEditarUsuario()
        {
            InitializeComponent();
            
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            txtNome_detalhe.Enabled = true;
            txtEmail_detalhe.Enabled = true;
            txtUserCadastro_detalhe.Enabled = true;
            chkAdm_consulta.Enabled = true;
            cboSetor_detalhe.Enabled = true;
            cboCargo_detalhe.Enabled = true;
            tsbSalvar.Enabled = true;
        }

        private void tsbSalvar_Click(object sender, EventArgs e)
        {
            SplashScreenManager.ShowForm(typeof (ucCarregando));
            conta.Nome = txtNome_detalhe.Text;
            conta.Usuario = txtUserCadastro_detalhe.Text;
            conta.Email = txtEmail_detalhe.Text;
            conta.Administrador = chkAdm_consulta.Checked;
            conta.Cargo = cboCargo_detalhe.SelectedItem.ToString();
            conta.Setor = cboSetor_detalhe.SelectedItem.ToString();

            if (conta.updateConta())
            {
                SplashScreenManager.CloseForm();
                MessageBox.Show(string.Format("Cadastro de {0} atualizado com sucesso", conta.Nome),
                    "Atualização de Cadastro");
            }
            else
            {
                SplashScreenManager.CloseForm();
                MessageBox.Show(string.Format("Erro na atualização do cadastro de {0}", conta.Nome),
                    "Atualização de Cadastro");
            }
        }
    }
}