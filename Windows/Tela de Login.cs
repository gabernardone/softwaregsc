﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using DevExpress.XtraSplashScreen;
using Windows.Users_Constrols;
using Microsoft.VisualBasic.PowerPacks;
using DevExpress.XtraEditors;


namespace Windows
{
    public partial class frmLogin : Form
    {
        int X = 0;
        int Y = 0;
        public static string tspUsuario_logado;

        public bool IsOnline()
        {
            try
            {
                System.Net.IPHostEntry dummy = System.Net.Dns.GetHostEntry("www.google.com"); //using System.Net;
                return true;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void TimerConexao_Tick(System.Object sender, System.EventArgs e)
        {
            if (IsOnline())
            {
                lblStatus1.Text = "Conectado!";
                picStatusConexao.Image = Windows.Properties.Resources.status_online;
            }
            else
            {
                picStatusConexao.Image = Windows.Properties.Resources.status_busy;
                lblStatus1.Text = "Desconectado! Verifique se há conexões existentes.";
            }
        }


        private void frmLogin_Load(Object sender, EventArgs e)
        {
            Thread.Sleep(4000);
        }

        public frmLogin()
        {
            InitializeComponent();
            this.MouseDown += new MouseEventHandler(frmLogin_MouseDown);
            this.MouseMove += new MouseEventHandler(frmLogin_MouseMove);
            txtSenha.GotFocus += txtSenha_GotFocus;
            txtUsuario.GotFocus += txtUsuario_GotFocus;

            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            lblVersion.Text = string.Format("Versão {0}", version);
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSobre sobre = new frmSobre();
            sobre.ShowDialog();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtUsuario.Clear();
            txtSenha.Clear();
        }

        public static void NewPoint(RectangleShape usuario, RectangleShape senha, Panel panel, Label label,
            SimpleButton entrar, SimpleButton cancelar, string error)
        {
            usuario.Visible = true;
            senha.Visible = true;
            panel.Visible = true;
            label.Text = error;
            var pointentrar = new Point(236, 132);
            entrar.Location = pointentrar;
            var pointcancelar = new Point(106, 132);
            cancelar.Location = pointcancelar;
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            SplashScreenManager.ShowForm(typeof (ucCarregando));
            tspUsuario_logado = txtUsuario.Text;
            Classes.Contas Account = new Classes.Contas();


            if ((txtUsuario.Text != String.Empty) && (txtSenha.Text != String.Empty))
            {
                if (Account.UsuarioExiste(txtUsuario.Text, txtSenha.Text))
                {
                    this.Dispose();
                    SplashScreenManager.CloseForm();
                    TelaPrincipal FormBuffer = new TelaPrincipal();
                    FormBuffer.ShowDialog();
                }
                else
                {
                    NewPoint(retsUsuario, retsSenha, panErro, lblErro, btnEntrar, btnCancelar,
                        "Usuário ou senha não autenticados!");
                    SplashScreenManager.CloseForm();
                }
            }
            else
            {
                NewPoint(retsUsuario, retsSenha, panErro, lblErro, btnEntrar, btnCancelar,
                    "Por favor preencher usuário e senha!");
                SplashScreenManager.CloseForm();
            }
        }

        private void txtSenha_GotFocus(object sender, EventArgs e)
        {
            if (panErro.Visible == true)
            {
                panErro.Visible = false;
                //Entrar
                var pointentrar_original = new Point(236, 114);
                this.btnEntrar.Location = pointentrar_original;

                //Cancelar
                var pointcancelar_original = new Point(106, 114);
                this.btnCancelar.Location = pointcancelar_original;
            }
        }

        private void txtUsuario_GotFocus(object sender, EventArgs e)
        {
            if (panErro.Visible == true)
            {
                panErro.Visible = false;
                //Entrar
                var pointentrar_original = new Point(236, 114);
                this.btnEntrar.Location = pointentrar_original;

                //Cancelar
                var pointcancelar_original = new Point(106, 114);
                this.btnCancelar.Location = pointcancelar_original;
            }
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnEntrar.PerformClick(); //Simula um clique no btnEntrar
            }
        }

        private void frmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            X = this.Left - MousePosition.X;
            Y = this.Top - MousePosition.Y;
        }

        private void frmLogin_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            this.Left = X + MousePosition.X;
            this.Top = Y + MousePosition.Y;
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmRecuperar_Senha recuperar = new frmRecuperar_Senha();
            recuperar.ShowDialog();
        }

        private void picShowHide_Click(object sender, EventArgs e)
        {
            if (txtSenha.UseSystemPasswordChar)
            {
                txtSenha.UseSystemPasswordChar = false;
                picShowHide.Image = Properties.Resources.Hide_16x16;
            }
            else
            {
                txtSenha.UseSystemPasswordChar = true;
                picShowHide.Image = Properties.Resources.Show_16x16;
            }
        }
    }
}